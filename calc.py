def suma(a, b):
    return a + b
def resta(a, b):
    return a - b

print("Suma: 1 + 2 =", suma(1, 2))
print("Suma: 3 + 4 =", suma(3, 4))
print("Resta: 6 - 5 =", resta(6, 5))
print("Resta: 8 - 7 =", resta(8, 7))